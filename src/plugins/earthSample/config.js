/*
 * @Description: 
 * @Version: 1.668
 * @Autor: 地虎降天龙
 * @Date: 2023-11-01 09:49:28
 * @LastEditors: 地虎降天龙
 * @LastEditTime: 2023-11-04 16:36:44
 */
export default {
	"name": "earthSample",
	"title": "地球的简单例子",
	"intro": "基于threeJS简单地球的例子",
	"version": "0.0.1",
	"author": "地虎降天龙",
	"website": "www.icegl.cn",
	"state": "active",
	"require": [],
	"preview": [
		{ "src": "plugins/earthSample/preview/earthA.png", "type": "img", "name": "earthA", "title": "样式A" },
		{ "src": "plugins/earthSample/preview/menuA.png", "type": "img", "name": "menuA", "title": "菜单A" },
		{ "src": "plugins/earthSample/preview/lowpolyPlanet.png", "type": "img", "name": "lowpolyPlanet", "title": "低像素多边形" }
	]
}