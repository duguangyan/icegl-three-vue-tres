/*
 * @Description: 
 * @Version: 1.668
 * @Autor: 地虎降天龙
 * @Date: 2023-10-31 15:33:02
 * @LastEditors: 地虎降天龙
 * @LastEditTime: 2023-11-04 16:36:40
 */

export default {
	"name": "vantaJS",
	"title": "vantaJS",
	"intro": "基于threeJS的炫酷背景墙",
	"version": "0.0.1",
	"author": "地虎降天龙",
	"website": "www.icegl.cn",
	"state": "active",
	"require": [],
	"preview": [
		{ "src": "plugins/vantaJS/preview/loadingA.png", "type": "img", "name": "loadingA", "title": "载入中A" }
	]
}