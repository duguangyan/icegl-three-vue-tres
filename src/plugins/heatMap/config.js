/*
 * @Description: 
 * @Version: 1.668
 * @Autor: 地虎降天龙
 * @Date: 2023-11-01 09:49:28
 * @LastEditors: 地虎降天龙
 * @LastEditTime: 2023-11-07 10:54:12
 */
export default {
	"name": "heatMap",
	"title": "热流图实例",
	"intro": "基于threeJS的热力图例子",
	"version": "0.0.1",
	"author": "地虎降天龙",
	"website": "www.icegl.cn",
	"state": "active",
	"require": [],
	"preview": [
		{ "src": "plugins/heatMap/preview/simpleExample.png", "type": "img", "name": "simpleExample", "title": "简单例子" },
		{ "src": "plugins/heatMap/preview/heatmapExample.png", "type": "img", "name": "heatmapExample", "title": "heatmap.js例子" }
	]
}