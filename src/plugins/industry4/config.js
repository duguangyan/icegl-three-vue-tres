/*
 * @Description: 
 * @Version: 1.668
 * @Autor: 地虎降天龙
 * @Date: 2023-11-10 16:11:27
 * @LastEditors: 地虎降天龙
 * @LastEditTime: 2023-11-29 12:38:33
 */

export default {
	"name": "industry4",
	"title": "工业4.0",
	"intro": "工业4.0数字化例子",
	"version": "0.0.1",
	"author": "地虎降天龙",
	"website": "www.icegl.cn",
	"state": "active",
	"require": [],
	"preview": [
		{ "src": "plugins/industry4/preview/deviceLight.png", "type": "img", "name": "deviceLight", "title": "设备发光" },
		// { "src": "plugins/industry4/preview/deviceLight.png", "type": "img", "name": "deviceLightByComposerTres", "title": "发光后期useTres" },
	]
}