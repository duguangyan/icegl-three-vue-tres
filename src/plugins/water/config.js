/*
 * @Description: 
 * @Version: 1.668
 * @Autor: 地虎降天龙
 * @Date: 2023-11-10 16:11:27
 * @LastEditors: 地虎降天龙
 * @LastEditTime: 2023-12-01 17:22:22
 */

export default {
	"name": "water",
	"title": "水相关",
	"intro": "河流、水域、海洋等场景",
	"version": "0.0.1",
	"author": "地虎降天龙",
	"website": "www.icegl.cn",
	"state": "active",
	"require": [],
	"preview": [
		{ "src": "plugins/water/preview/tilingCaustics.png", "type": "img", "name": "tilingCaustics", "title": "波纹A" },
		{ "src": "plugins/water/preview/tilingCaustics.png", "type": "img", "name": "waterGlass", "title": "波浪A" },
	]
}