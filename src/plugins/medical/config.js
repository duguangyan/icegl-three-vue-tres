/*
 * @Description: 
 * @Version: 1.668
 * @Autor: 地虎降天龙
 * @Date: 2023-11-10 16:11:27
 * @LastEditors: 地虎降天龙
 * @LastEditTime: 2023-11-14 16:38:55
 */

export default {
	"name": "medical",
	"title": "医疗行业",
	"intro": "医疗行业数字化例子",
	"version": "0.0.1",
	"author": "地虎降天龙",
	"website": "www.icegl.cn",
	"state": "active",
	"require": [],
	"preview": [
		{ "src": "plugins/medical/preview/digitalBrain.png", "type": "img", "name": "digitalBrain", "title": "数字大脑" },
	]
}