/*
 * @Description: 
 * @Version: 1.668
 * @Autor: 地虎降天龙
 * @Date: 2023-10-26 09:20:42
 * @LastEditors: 地虎降天龙
 * @LastEditTime: 2023-11-28 16:53:05
 */
export default {
	"name": "digitalCity",
	"title": "数字城市",
	"intro": "基于城市场景的可视化展示插件",
	"version": "0.0.1",
	"author": "地虎降天龙",
	"state": "active",
	"require": ['cannon-es'],
	"preview": [
		// { "src": "plugins/digitalCity/preview/buildings.mp4", "type": "video", "name": "buildings", "title": "建筑物" },
		{ "src": "plugins/digitalCity/preview/buildings.png", "type": "img", "name": "buildings", "title": "建筑物" },
		{ "src": "plugins/digitalCity/preview/radars.png", "type": "img", "name": "radars", "title": "雷达" },
		{ "src": "plugins/digitalCity/preview/weather.png", "type": "img", "name": "weather", "title": "天气" },
		{ "src": "plugins/digitalCity/preview/fireA.png", "type": "img", "name": "fireA", "title": "火A效果" },
		{ "src": "plugins/digitalCity/preview/heatmap.png", "type": "img", "name": "heatmap", "title": "热力图" },
		{ "src": "plugins/digitalCity/preview/heatmap2.png", "type": "img", "name": "heatmap2", "title": "建筑物-热力图" },
	]
}